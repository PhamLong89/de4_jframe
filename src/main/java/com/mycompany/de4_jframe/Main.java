/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de4_jframe;

import javax.swing.JFrame;

/**
 *
 * @author long
 */
public class Main extends JFrame {

    public Main() {
        insertAndShow();
    }

    private void insertAndShow() {
        settingsJFrame();
    }

    private void settingsJFrame() {
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setTitle("Quản lý cửa hàng");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        new Main().setVisible(true);
    }
}
